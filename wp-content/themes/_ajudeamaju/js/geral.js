$(function(){


	/*****************************************
		CARROSSEIS
	*******************************************/
		//CAROUSEL PHOTOS MAJU
		$("#carouselPhotosMaju").owlCarousel({
			items : 5,
	        dots: false,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,
	        autoplay:true,	       
		    autoplayTimeout:1500,
		    autoplayHoverPause:true,
		    smartSpeed: 450,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	        	1:{
	                items:1
	            },
	            320:{
	                items:1
	            },
	            445:{
	                items:2
	            },
	            600:{
	                items:3
	            },
	            767:{
	                items:3
	            },
	           
	            991:{
	                items:4
	            },
	            1024:{
	                items:5
	            },
	            1440:{
	                items:5
	            },
	            			            
	        }		    		   		    
		    
		});

		$("#carrosselProdutos").owlCarousel({
			items:3,
	        dots:false,
	        loop:false,
	        lazyLoad:true,
	        mouseDrag:false,
	        touchDrag:false,
	        autoplay:false,	     
		    //center:true,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	        	1:{
	                items:1
	            },
	            600:{
	                items:1
	            },
	            767:{
	                items:1
	            },
	           
	            991:{
	                items:1
	            },
	            1024:{
	                items:1
	            },
	            1250:{
	                items:1
	            },
	            1440:{
	                items:1
	            },
	            			            
	        }  		   		    
		    
		});
		
	/*****************************************
		SCRIPTS GERAIS
	*****************************************/
		$('header .navbar button').click(function() {
	 		let verif = $(this).attr('aria-expanded');
	 		if(verif == "true" || verif == "undefined"){
	 			$(this).removeClass('exchangeButton');
	 		}else{
	 			$(this).addClass('exchangeButton');
	 		}
	 		
	 		setTimeout(function(){
	 			if(verif == "true" || verif == "undefined"){
	 				$(this).removeClass('exchangeButton');
	 			}else{
	 				$(this).addClass('exchangeButton');
	 			}
	 		}, 100);
	 	});


		$("a.fancy").fancybox({
			'titleShow' : false,
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			closeBtn    : true,
			arrows      : true,
			nextClick   : true
		});

		$('a.anchor').click(function() {
			$(".navbar-collapse").removeClass('in');
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});

		//MUDAR TEXTO DO BOTÃO COMPRAR PARA APADRINHAR
		//$('.pg-inicial .howToHelp .productsSection .productList .product .single_add_to_cart_button.button.alt').text('Apadrinhar');

		//TROCAR CAMPO (Number) PARA (Número)
		// var numero = $("#billing_number_field label").text();
		// numero = numero.replace("Number", "Número");
		// $('#billing_number_field label').text(numero);

		// var referencia = $("#billing_neighborhood_field label").text();
		// referencia = referencia.replace("Number", "Número");
		// $('#billing_number_field label').text(referencia);


		// $(".pg-inicial .photoArea .carouselPhotosMaju .item.photo").click(function(){
		// 	var imagemInteira = $(this).children().children().attr('data-galeria');
		// 	var html = '<img src="'+imagemInteira+ '" alt="Ajude a Maju">';
		// 	$('body').addClass('travarScroll');
		// 	$(".galeryMaju").append(html);
		// 	$(".galeryMaju").fadeIn();
		// });

		// $(".galeryMaju span.fecharModal").click(function(){
		// 	$('body').removeClass('travarScroll');
		// 	$(".galeryMaju").fadeOut();
		// 	$(".galeryMaju img").remove();

		// });

		// $(document).keyup(function(e) {
		// 	if (e.keyCode == 27) {
		// 		$('body').removeClass('travarScroll');
		// 		$(".galeryMaju").fadeOut();
		// 		$(".galeryMaju img").remove();
		// 	}
		// });
});