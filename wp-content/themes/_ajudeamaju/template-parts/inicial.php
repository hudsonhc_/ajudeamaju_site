<?php
/**
 * Template Name: Inicial
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ajudeamaju
 */
global $product;
get_header();
?>
	<!-- PG INICIAL -->
	<div class="pg pg-inicial">
		<section class="apresentacaoInicial">
			<div class="row">
				<div class="col-sm-6">
					<div class="imagemMaju">
						<figure>
							<img src="<?php echo $configuracao['opt_imagem_inicial_esquerda']['url'] ?>" alt="Maju">
						</figure>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="textoInicialDireita">
						<?php $textos = $configuracao['opt_inicial_texto'];  ?>
						<h1><?php echo $textos; ?></h1>
					</div>
				</div>
			</div>
		</section>
		<section class="descriptionArea">
			<h6 class="hidden">Description Area</h6>
			<div class="descriptionSkew">
				<div class="descriptionAntiSkew">
					<div class="row">
						<div class="col-sm-8">
							<div class="helpText">
								<?php echo $configuracao['opt_segunda_sessao_texto_esquerda'] ?>
							</div>
						</div>	
						<div class="col-sm-4">
							<div class="helpImage">
								<figure>
									<img src="<?php echo $configuracao['opt_segunda_sessao_imagem_direita']['url'] ?>" alt="Baby">
								</figure>
							</div>
						</div>
					</div>
					<div class="beaGodfather">
						<a class="anchor" href="#comoapadrinhar">Você aceita ser meu padrinho mesmo assim?</a>
					</div>
				</div>
			</div>
		</section>
		<section class="howToHelp">
			<div class="thankYou">
				<h2 id="comoapadrinhar"><?php echo $configuracao['opt_sessao_padrinho_titulo']; ?></h2>
			</div>
			<div class="descricaoApadrinhamento">
				<?php echo $configuracao['opt_sessao_padrinho_descricao']; ?>
				<!-- <p>O que é apadrinhamento?</p>
				<p>Apadrinhar a Maju é contribuir com uma pequena quantia mensal, para atender as necessidades básicas dela, curativos, pomadas, remédios, plano de saúde...</p>
				<p>Você pode interromper a qualquer momento o apadrinhamento. Ou seja, caso você esteja passando por um período de dificuldades financeiras, pode cancelar o apadrinhamento, e retornar posteriormente.</p>
				<p>Nos ajude a cuidar da nossa borboleta, precisamos de muitos guardiões para que ela possa bater as asas e voar!!!</p> -->
			</div>
			<div class="productsSection">
				<div class="productList" id="carrosselProdutos">
				<?php
						$args = array(
						    'post_type'      => 'product',
						    'posts_per_page' => -1,
						    'meta_query'     => array(
						        'relation' => 'OR',
						    )
						);

						$loop = new WP_Query( $args );

			   			while ( $loop->have_posts() ) : $loop->the_post();
			   			 global $product;
			   			 $nomeProduto = $product->name;
			   			 $precoInicial = str_replace('.', ',', $product->price);
			   			 $precoProduto = explode('.', $product->price);
			   			 $precoProduto = $precoProduto[0];
			   			 $descricaoProduto = $product->short_description;

			   			        
				   			// $fotoProduto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
							// $fotoProduto = $fotoProduto[0];
						

					?>
					<div class="item">
						<div class="product">
							<span class="price">+<?php if($precoProduto){echo $precoProduto;} ?></span>
							<h3 class="productTitle"><?php 
								if($precoInicial){
									echo $nomeProduto.' Com R$'.$precoInicial; 
								}
								else{
									echo $nomeProduto;
								} ?></h3>
							<p class="productDescription">
								<?php echo $descricaoProduto; ?>
							</p>
							<?php
											/**
											 * woocommerce_after_shop_loop_item hook
											 *
											 * @hooked woocommerce_template_loop_add_to_cart - 10
											 */
								do_action( 'woocommerce_after_shop_loop_item' );
								woocommerce_template_single_add_to_cart(); ?>
						</div>
					</div>
				<?php endwhile; wp_reset_query(); ?>
				</div>
			</div>
		</section>

		<section class="photoArea">
			<h6 class="hidden">Área de fotos</h6>
			
			<div id="carouselPhotosMaju" class="carouselPhotosMaju">
				<?php
					$imagens = explode(',', $configuracao['opt_fotos_imagens']);
				
					foreach ($imagens as $imagens ):
						$imagemCarrossel = wp_get_attachment_url($imagens);
				?>
				<div class="item photo">
					<a href="<?php echo $imagemCarrossel ?>" class="fancy" rel="gallery1">
						<figure>
							<img src="<?php echo $imagemCarrossel ?>" alt="Maju" data-galeria="<?php echo $imagemCarrossel; ?>">
						</figure>
					</a>
				</div>
				<?php endforeach; ?>
			</div>
		</section>
		<div class="galeryMaju" style="display: none;"><span class="fecharModal"><i class="fas fa-times"></i></span>
		</div>
	</div>
<?php get_footer(); ?>