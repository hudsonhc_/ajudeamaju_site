$(function(){


	/*****************************************
		CARROSSEIS
	*******************************************/
		//CAROUSEL PHOTOS MAJU
		$("#carouselPhotosMaju").owlCarousel({
			items : 5,
	        dots: false,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,
	        autoplay:true,	       
		    autoplayTimeout:1500,
		    autoplayHoverPause:true,
		    smartSpeed: 450,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	        	1:{
	                items:1
	            },
	            320:{
	                items:1
	            },
	            445:{
	                items:2
	            },
	            600:{
	                items:3
	            },
	            767:{
	                items:3
	            },
	           
	            991:{
	                items:4
	            },
	            1024:{
	                items:5
	            },
	            1440:{
	                items:5
	            },
	            			            
	        }		    		   		    
		    
		});

		$("#carrosselProdutos").owlCarousel({
			items:3,
	        dots:false,
	        loop:false,
	        lazyLoad:true,
	        mouseDrag:false,
	        touchDrag:false,
	        autoplay:false,	     
		    //center:true,

		    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	        	1:{
	                items:1
	            },
	            600:{
	                items:1
	            },
	            767:{
	                items:1
	            },
	           
	            991:{
	                items:1
	            },
	            1024:{
	                items:1
	            },
	            1250:{
	                items:1
	            },
	            1440:{
	                items:1
	            },
	            			            
	        }  		   		    
		    
		});
		
	/*****************************************
		SCRIPTS GERAIS
	*****************************************/
		$('header .navbar button').click(function() {
	 		let verif = $(this).attr('aria-expanded');
	 		if(verif == "true" || verif == "undefined"){
	 			$(this).removeClass('exchangeButton');
	 		}else{
	 			$(this).addClass('exchangeButton');
	 		}
	 		
	 		setTimeout(function(){
	 			if(verif == "true" || verif == "undefined"){
	 				$(this).removeClass('exchangeButton');
	 			}else{
	 				$(this).addClass('exchangeButton');
	 			}
	 		}, 100);
	 	});


		$("a.fancy").fancybox({
			'titleShow' : false,
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			closeBtn    : true,
			arrows      : true,
			nextClick   : true
		});

		$('a.anchor').click(function() {
			$(".navbar-collapse").removeClass('in');
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});
});