<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'abraceam_pagina_padrinhos');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'abraceam_root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'a@hc1468');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '14knvRDM<*%_o.FI!G6>n}4HAX/HOaxA+2}.=+h!~hD077Hw_xd2uXD81[!JOO&M');
define('SECURE_AUTH_KEY',  'm6S4.w&qKeF|z|T[Ti1~E41~;bZLzk=A^i;:57>~1L/UT3S.:zP|CkdGaZ=]E:2J');
define('LOGGED_IN_KEY',    'b^1W|:ouIV:-*l&Y$;I.KHQGKqYk4p7iczJ>PURM*`$>)>2aOe7^~ QgGB8w%4Va');
define('NONCE_KEY',        '>H!*wynjwTDf^k8S)Z0>M?OU3o5IrK{` l9+=kB1{dEq+yoG*5[lJ:l+]8l.]`(7');
define('AUTH_SALT',        'EqcW}eqq<6k9bGl_WDb}d0zja$qN|E};Ae`M)k:Rs%_)+/}a:@K:I&L]i(4e)rZ!');
define('SECURE_AUTH_SALT', 'UG]q-sB0#gyNM@VNvw0t&8ON)>ZFP}OxTX9_O7zzCOh_V}1L<d5KPw9|cC*%5)2s');
define('LOGGED_IN_SALT',   '}_ :>eZZz>%PvXglss8cB87V!7f*&KCNtzKkiyRc`}>P44(NjFn2.EnhMLe^v/0_');
define('NONCE_SALT',       'zI!9&A>8^dz]6VQ2:o<8iB|?/mwmnb8!hWf_q|(kp[{s<Dn>LXm62g (6$)f^H]%');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'am_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
